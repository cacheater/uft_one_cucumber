﻿
'Precondiciones: Usuario sin inicar sesion
'Fecha: 27/01/2021
'Tester: Fabian Solano

'Feature: Compras
' Como cliente de amazon quiero hacer busquedas de prodcutos para agregar al carrito

  'Scenario: Agregar productos al carrito

    'Given El cliente se encuentra en la pagina amazon.com.mx
    Call fnGivenElClienteSeEncuentraEnLaPaginaAmazon()

    'When El cliente realiza una busqueda de un producto
    Call fnWhenElClienteRealizaUnaBusquedaDeUnProducto()

    'Then Se muestran resultados relacionados con la busqueda
    Call fnThenSeMuestranResultadosRelacionadosConLaBusqueda()

    'And Selecciono el primer producto de la lista
    Call fnAndSeleccionoElPrimerProductoDeLaLista()

    'And Se muestran los detalles del producto
    Call fnAndSeMuestranLosDetallesDelProducto()

    'And Selecciono agregar al carrito
    Call fnAndSeleccionoAgregarAlCarrito()

    'Then Se actualiza el estado de mi carrito
    Call fnThenSeActualizaElEstadoDeMiCarrito()
 
